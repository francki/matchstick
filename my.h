#ifndef _MY_H
#define _MY_H

#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"

void my_putchar(char c);
int my_putstr(char *str);
char **matchstick(int line, int stick);
int my_getnbr(char const *str);
int printgnl(int line, int stick, char **tab);
int my_putnbr(int nb);
int error(int line, int stick);
char **pipeciao(char **tab, int match, int l);
void printab(char **tab, int line, int nb);
int gest_one(char **tab, int match, int l);
int line_empty(char **tab, int l);
int error_ingame(int l, int match, int stick, int line);
int check_str(char *str);
int comptline(char **tab, int match, int l);

//printf

void flag_d(va_list ap);
void flag_s(va_list ap);
void flag_c(va_list ap);
void my_printf(char *str, ...);
int checkflag(char c);
void my_ptab(int i, va_list (ap));

#endif /*READ_SIZE*/
