#include "get_next_line.h"

void my_memcpy(char *dest, char *src, int size)
{
	int i = -1;

	while (++i < size)
		dest[i] = src[i];
}

char *my_strcat(const char *s1, const char *s2)
{
	int i = -1;
	int j = -1;
	char *result;

	while (s1 && s1[++i]);
	while (s2 && s2[++j]);
	if ((result = malloc(sizeof(char) * (i + j + 1))) == NULL)
		return (NULL);
	i = -1;
	j = -1;
	while (s1[++i])
		result[++j] = s1[i];
	i = -1;
	while (s2[++i])
		result[++j] = s2[i];
	result[++j] = 0;
	return (result);
}

int line(char *str)
{
	int i = -1;

	while (str && str[++i])
		if (str[i] == '\n')
			return (1);
	return (0);
}

char *separbuff(char *str)
{
	char *res;
	int i = -1;
	int cnt = -1;

	while (str && str[++i] && str[i] != '\n');
	if (str[i] == 0)
		return (str);
	if ((res = malloc(sizeof(char) * (i + 1))) == NULL)
		return (NULL);
	my_memcpy(res, str, i);
	res[i] = 0;
	++i;
	while (str[++cnt + i])
		str[cnt] = str[cnt + i];
	str[cnt] = 0;
	return (res);
}

char *get_next_line(int fd)
{
	static char *buff = ""; char tmp[READ_SIZE]; int ret; int compt = -1;

	if (fd < 0)
		return (NULL);
	while (line(buff) != 1) {
		compt = -1;
		while (++compt < READ_SIZE)
			tmp[compt] = '\0';
		if ((ret = read(fd, tmp, READ_SIZE)) == 0) {
			if (buff[0] != '\0')
				return (buff);
			else
				return (NULL);
		}
		tmp[ret] = '\0';
		if (buff != NULL) {
			buff = my_strcat(buff, tmp);
			if (buff == NULL)
				return (NULL);
		}
	}
	return (separbuff(buff));
}
