#include "my.h"

char	**matchstick(int line, int stick)
{
	char **tab;
	int i = 0;
	int nb = 1;
	int x = -1;
	int space = 0;
	int star = 0;

	tab = malloc(sizeof(char*) * line + sizeof(char*));
	while (line != i) {
		tab[i] = malloc(sizeof(char) * nb + 1);
		nb = nb + 2;
		i++;
	}
	i = 0;
	nb = 1;
	x = -1;
	while (tab[++x]) {
		while (i != nb) {
			tab[x][i] = '|';
			i++;
		}
		tab[x][i] = '\0';
		nb = nb + 2;
		i = 0;
	}
	line = line - 1;
	x = -1;
	printab(tab, line, nb);
	return (tab);
}

void printab(char **tab, int line, int nb)
{
	int star = 0;
	int x = -1;
	int space = 0;

	while (star <= (nb - 1)) {
		my_putchar('*');
		star++;
	}
	my_putchar('\n');
	while (tab[++x]) {
		my_putchar('*');
		while (line != space) {
			my_putchar(' ');
			space++;
		}
		space = 0;
		my_putstr(tab[x]);
		while (line != space) {
			my_putchar(' ');
			space++;
		}
		my_putchar('*');
		my_putchar('\n');
		line = line - 1;
		space = 0;
	}
	star = 0;
	while (star <= (nb - 1)) {
		my_putchar('*');
		star++;
	}
	my_putchar('\n');
}

int printgnl(int line, int stick, char **tab)
{
	int match = 0;
	int l = 0;
	int nb = 1;
	int x = - 1;
	int error = 1;
	char *str;

	while (tab[++x])
		nb = nb + 2;
	while (error == 1) {
		my_putstr("Line: ");
		str = get_next_line(0);
		l = my_getnbr(str);
		if (check_str(str) == 84) {
			my_putstr("Error: invalid input (positive number expected)\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 84) {
			my_putstr("This line is out of range\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 79) {
			my_putstr("This line is out of range\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 80) {
			my_printf("Error: invalid input (positive number expected)\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (line_empty(tab, l) == 1) {
			my_putstr("Error: not enough matches on this line\n");
			printgnl(line, stick, tab);
			return (0);
		}
		my_putstr("Matches: ");
		str = get_next_line(0);
		match = my_getnbr(str);
		if (check_str(str) == 84) {
			my_putstr("Error: invalid input (positive number expected)\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 84) {
			my_putstr("This line is out of range\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 83) {
			my_putstr("You have to remove at least one match\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 82) {
			my_printf("You cannot remove more than %d matches per turn\n", stick);
			printgnl(line, stick, tab);
			return (0);
		}
		if (error_ingame(l, match, stick, line) == 81) {
			my_printf("Error: invalid input (positive number expected)\n");
			printgnl(line, stick, tab);
			return (0);
		}
		if (gest_one(tab, match, l) == 1) {
			my_putstr("Error: not enough matches on this line\n");
			printgnl(line, stick, tab);
			return (0);
		}
		write(1, "Player removed ", 15);
		my_putnbr(match);
		write(1, " match(es) from line ", 21);
		my_putnbr(l);
		write(1, " \n", 2);
		tab = pipeciao(tab, match, l);
		printab(tab, line, nb + 2);
		error = 0;
	}
}

int check_str(char *str)
{
	int i = 0;

	while (str[i]) {
		if (str[i] < '0' || str[i] > '9')
			return (84);
		i++;
	}
	return (i);
}

char **pipeciao(char **tab, int match, int l)
{
	int x = -1;
	int new = 0;
	int check = 0;

	while (tab[l - 1][++x] != '|');
	while (tab[l - 1][++x] == '|');
	x = x -1;
	while (new < match) {
		tab[l - 1][x] = ' ';
		x--;
		new++;
	}
	return (tab);
}

int comptline(char **tab, int match, int l)
{
	int x = 0;
	int compt_lines = 0;

	while (tab[l -1][x] != '\0') {
		if (tab[l -1][x] == '|') {
			l++;
			compt_lines++;
		}
		x++;
	}
	l++;
	return (0);
}

int gest_one(char **tab, int match, int l)
{
	int x = 0;
	int check = 0;

	while (tab[l - 1][x] != '\0') {
		if (tab[l - 1][x] == '|') {
			check++;
		}
		x++;
	}
	if (check < match) {
		return (1);
	}
	return (0);
}

int line_empty(char **tab, int l)
{
	int x = 0;
	int check = 0;

	while (tab[l - 1][x] != '\0') {
		if (tab[l - 1][x] == '|') {
			check++;
		}
		x++;
	}
	if (check == 0)
		return (1);
	return (0);
}

int error(int line, int stick)
{
	if (line < 1 || line > 100 || stick < 1)
		return (84);
	if (stick > 100 || stick < 1)
		return (84);
}

int error_ingame(int l, int match, int stick, int line)
{
	if (match < 0)
		return (81);
	if (l > line)
		return (84);
	if (l < 0)
		return (80);
	if (l == 0)
		return (79);
	if (match > stick)
		return (82);
	if (match == 0)
		return (83);
}

int main(int ac, char **av)
{
	char **tab;
	int line;
	int stick;
	char *str;
	int l;
	int match;

	if (ac != 3)
		return (84);
	line = my_getnbr(av[1]);
	stick = my_getnbr(av[2]);
	if (error(line, stick) == 84)
		return (84);
	tab = matchstick(line, stick);
	while (42) {
		my_putstr("Your turn:\n");
		printgnl(line, stick, tab);
	}
	return (0);
}
