#include "my.h"

int my_putnbr(int nb)
{
	int i = 0;

	if (nb == -2147483648)
		return (write(1, "-2147483648", 11));
	while (nb < 0) {
		my_putchar('-');
		nb = (nb * (-1));
	}
	if (nb >= 10) {
		i = (nb % 10);
		nb = (nb - i) / 10;
		my_putnbr(nb);
		my_putchar(i + 48);
	}
	else
		my_putchar(nb + 48);
	return (0);
}
