int	my_getnbr(char const *str)
{
	long	save = 0;
	int	o = 1;

	while (*str != '\0') {
		if (*str == '-')
			o = o * -1;
		if (*str >= '0' && *str <= '9') {
			while (*str >= '0' && *str <= '9') {
				save = (save * 10) + *str - '0';
				*str++;
			}
			save = save * o;
			if (save <= 2147483647 && save >= -2147483648)
				return (save);
			else
				return (0);
		}
		*str++;
	}
	return (0);
}
