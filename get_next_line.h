#ifndef GET_NEXT_LINE_
# define GET_NEXT_LINE_
#ifndef READ_SIZE
# define READ_SIZE (10)
#endif /*READ_SIZE*/

# include <stdlib.h>
# include <time.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>


void	my_memcpy(char *dest, char *src, int size);
char	*my_strcat(const char *s1, const char *s2);
int	line(char *str);
char	*separbuff(char *str);
char	*get_next_line(int fd);

#endif /*GET_NEXT_LINE*/
